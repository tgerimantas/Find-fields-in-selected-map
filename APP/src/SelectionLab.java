
import java.awt.*;
import java.awt.event.*;
import java.net.URL;

import javax.swing.*;

import org.geotools.data.*;
import org.geotools.data.collection.ListFeatureCollection;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.process.vector.IntersectionFeatureCollection;
import org.geotools.referencing.CRS;
import org.geotools.referencing.GeodeticCalculator;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.geotools.renderer.GTRenderer;
import org.geotools.renderer.lite.StreamingRenderer;
import org.geotools.styling.LineSymbolizer;
import org.geotools.styling.PolygonSymbolizer;
import org.geotools.styling.Style;
import org.geotools.styling.StyleBuilder;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;    

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.geotools.data.DataStore;
import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.Query;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.filter.text.cql2.CQL;
import org.geotools.filter.text.cql2.CQLException;
import org.geotools.geometry.DirectPosition2D;
import org.geotools.geometry.iso.topograph2D.Envelope;
import org.geotools.geometry.jts.JTS;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.grid.DefaultGridFeatureBuilder;
import org.geotools.grid.Envelopes;
import org.geotools.grid.GridFeatureBuilder;
import org.geotools.grid.Grids;
import org.geotools.grid.oblong.Oblongs;
import org.geotools.map.FeatureLayer;
import org.geotools.map.Layer;
import org.geotools.map.MapContent;
import org.geotools.map.event.MapLayerListEvent;
import org.geotools.map.event.MapLayerListListener;
import org.geotools.renderer.lite.StreamingRenderer;
import org.geotools.styling.FeatureTypeStyle;
import org.geotools.styling.Fill;
import org.geotools.styling.Graphic;
import org.geotools.styling.Mark;
import org.geotools.styling.Rule;
import org.geotools.styling.Stroke;
import org.geotools.styling.Style;
import org.geotools.styling.StyleFactory;
import org.geotools.styling.Symbolizer;
import org.geotools.swing.JMapFrame;
import org.geotools.swing.action.PanAction;
import org.geotools.swing.action.SafeAction;
import org.geotools.swing.action.ZoomInAction;
import org.geotools.swing.action.ZoomOutAction;
import org.geotools.swing.data.JFileDataStoreChooser;
import org.geotools.swing.event.MapMouseEvent;
import org.geotools.swing.tool.CursorTool;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.type.AttributeDescriptor;
import org.opengis.feature.type.GeometryDescriptor;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;
import org.opengis.filter.identity.FeatureId;
import org.opengis.filter.identity.Identifier;
import org.opengis.geometry.MismatchedDimensionException;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.NoSuchAuthorityCodeException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;
import org.opengis.referencing.operation.TransformException;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.GeometryFactory;





import org.geotools.referencing.CRS;

















import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.opengis.*;
import org.geotools.*;


public class SelectionLab extends JFrame {
	public SelectionLab() {
	}
	
	private static StyleFactory sf = CommonFactoryFinder.getStyleFactory();
	private static FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();
	private enum GeomType { POINT, LINE, POLYGON };
	private static final Color LINE_COLOUR = Color.BLUE;
	private static final Color FILL_COLOUR = Color.CYAN;
	private static final Color SELECTED_COLOUR = Color.YELLOW;
	private static final float OPACITY = 1.0f;
	private static final float LINE_WIDTH = 1.0f;
	private static final float POINT_SIZE = 10.0f;
	public static JMapFrame mapFrame;
	private static SimpleFeatureSource featureSource;
	private static String geometryAttributeName;
	private static GeomType geometryType;
	private  QueryTable queryTable;
	public static HashSet<FeatureId> IDs;
	public MapContent map;
	public static ReferencedEnvelope envelope = null;
	public boolean selected = true;
	static Filter filter;
	Rectangle2D worldRect;
	static ReferencedEnvelope bbox;
	static ReferencedEnvelope bboxBOX;
	public ArrayList<FeatureId>list;
	private HashMap<Layer, HashSet<Identifier>> selectedFeatureIds;
	
	//Treciam
	Geometry allUnion = null;
	SimpleFeatureType featureType;
	SimpleFeatureCollection deletedFeature;
	SimpleFeatureSource deleteAreaSourceAll;
	SimpleFeatureSource keliaiTempSource;
	SimpleFeatureSource hidroTempSource;
	
	public void displayShapefile() throws Exception {
		
		map = new MapContent();
		map.setTitle("GIS pirmoji programa");  			//Programos pavadinimas
		mapFrame = new JMapFrame(map);
		mapFrame.enableToolBar(true);					//Tool baras, kuriame yra priartinimas 
		mapFrame.enableLayerTable( true );				//Layer baras, kuriame galima pasalinti layer, rodyti ar nerodyti
		//mapFrame.enableStatusBar(true);       		//Status bar , kuriame atvaizduojamas x, y koordinamtÄ—s
		
		DataStores st = new DataStores();
		JToolBar toolBar = mapFrame.getToolBar();
		IDs = new HashSet<FeatureId>();
		selectedFeatureIds = new HashMap<>();
		
		//Select mygtukas
		JButton btn = new JButton(new ImageIcon("C:\\Users\\Gerimantas\\workspace\\2_GIS\\src\\icons\\selected2.png"));
		btn.setToolTipText("ObjektÅ³ pasirinkimas");   
		toolBar.addSeparator();
		toolBar.add(btn);
		
		//PridÄ—ti naujÄ… layer mygtukas
		JButton btn1 = new JButton(new ImageIcon("C:\\Users\\Gerimantas\\workspace\\2_GIS\\src\\icons\\add186.png"));
		btn1.setToolTipText("Sluoksnio pridÄ—jimas");   
		toolBar.addSeparator();
		toolBar.add(btn1);
		
		//Priartinti paÅ¾ymÄ—tÄ… objetÄ…
		JButton btn2 = new JButton(new ImageIcon("C:\\Users\\Gerimantas\\workspace\\2_GIS\\src\\icons\\arrows1.png"));
		btn2.setToolTipText("ParinktÅ³ (paÅ¾ymÄ—tÅ³) objektÅ³ parodymas stambiu planu ");   
		toolBar.addSeparator();
		toolBar.add(btn2);
		
		//Atributai
		JButton btn3 = new JButton(new ImageIcon("C:\\Users\\Gerimantas\\workspace\\2_GIS\\src\\icons\\tables1.png"));	
		btn3.setToolTipText("AtributÅ³ lentelÄ—s parodymas");   
		toolBar.addSeparator();
		toolBar.add(btn3);
		
		
		JButton btn4 = new JButton(new ImageIcon("C:\\Users\\Gerimantas\\workspace\\2_GIS\\src\\icons\\road.png"));
		btn4.setToolTipText("II Bendra keliu tinklo ilgi (KELIAI) ir koks vidutinis keliu tinklo ilgis tenka vienam kvadratiniam kilometrui ");   
		toolBar.addSeparator();
		toolBar.add(btn4);
		
		JButton btn5 = new JButton(new ImageIcon("C:\\Users\\Gerimantas\\workspace\\2_GIS\\src\\icons\\area.png"));	
		btn5.setToolTipText("II Hidrografijos teritoriju plota ir t.t.  ");   
		toolBar.addSeparator();
		toolBar.add(btn5);
		
		JButton btn6 = new JButton(new ImageIcon("C:\\Users\\Gerimantas\\workspace\\2_GIS\\src\\icons\\building.png"));	
		btn6.setToolTipText("II Statiniu plotai ");   
		toolBar.addSeparator();
		toolBar.add(btn6);
		
		//Trecioji uzduotis surask man tinkama darza "Darzas"
		JButton btn7 = new JButton(new ImageIcon("C:\\Users\\Gerimantas\\workspace\\2_GIS\\src\\icons\\gardening.png"));
		btn7.setToolTipText("Pasirinktame plote randamas plotas darzui");   
		toolBar.addSeparator();
		toolBar.add(btn7);
			      
		for (int i = 0; i < toolBar.getComponentCount(); i++) {
            JButton button;
            try {
                button = (JButton)toolBar.getComponent(i);
            } catch (ClassCastException e) {
                toolBar.remove(i);
            }
        }
		
		JButton button0 = (JButton)toolBar.getComponent(0);
		button0.setToolTipText("Kursorius be paÅ¾yÄ—jimo");
		button0.setIcon(new ImageIcon("C:\\Users\\Gerimantas\\workspace\\2_GIS\\src\\icons\\cursor12.png"));
		
		JButton button1 = (JButton)toolBar.getComponent(1);
		button1.setToolTipText("Informacijos sluoksnio artinimas");
		button1.setIcon(new ImageIcon("C:\\Users\\Gerimantas\\workspace\\2_GIS\\src\\icons\\zoom75.png"));
		
		JButton button2 = (JButton)toolBar.getComponent(2);
		button2.setToolTipText("Informacijos sluoksnio tolinimas");
		button2.setIcon(new ImageIcon("C:\\Users\\Gerimantas\\workspace\\2_GIS\\src\\icons\\magnifying-glass2.png"));
		
		JButton button3 = (JButton)toolBar.getComponent(3);
		button3.setToolTipText("Informacijos sluoksnio vaizdo postÅ«mis ");
		button3.setIcon(new ImageIcon("C:\\Users\\Gerimantas\\workspace\\2_GIS\\src\\icons\\arrow639.png"));
		
		toolBar.remove(4);
		
		JButton button4 = (JButton)toolBar.getComponent(4);
		button4.setToolTipText("GraÅ¾inimas Ä¯ pradinÄ¯ vaizdÄ…/pilnos duomenÅ³ apimties ");
		button4.setIcon(new ImageIcon("C:\\Users\\Gerimantas\\workspace\\2_GIS\\src\\icons\\full12.png"));
		
		
		
		
		
	    btn.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                mapFrame.getMapPane().setCursorTool(
                        new CursorTool()
                        {
                            java.awt.Point startPos;
                            Rectangle rect;
                            
                            
                            //Padaro select border langa
                            @Override
                            public boolean drawDragBox() {
                                return true;
                            }
							
                            //Paspaudus ant Å¾emÄ—lapio gauname pradinÄ™ pozicijÄ…
                            @Override
                            public void onMousePressed(MapMouseEvent ev)
                            {
                                if (!ev.isControlDown()) //jeigu ne paspaudus ctrl
                                {
                                    IDs = new HashSet<FeatureId>();
                                }
                                startPos = new java.awt.Point(ev.getPoint());
                            }
                            
                            //Atleidus pelÄ™ gauname pabaigos pozicijÄ…
                            @Override
                            public void onMouseReleased(MapMouseEvent ev)
                            {
                                if (!startPos.equals(ev.getPoint())) //Kai paÅ¾ymime su pele
                                {
                                    rect = new Rectangle();
                                    rect.setFrameFromDiagonal(startPos, ev.getPoint());
                                    AffineTransform screenToWorld = mapFrame.getMapPane().getScreenToWorldTransform();
                                    Rectangle2D worldRect = screenToWorld.createTransformedShape(rect).getBounds2D();
                                    ReferencedEnvelope bbox = new ReferencedEnvelope(worldRect, mapFrame.getMapContent().getCoordinateReferenceSystem());
                                    bboxBOX=bbox;
                                    selectFeatures(bbox);
                                }
                                else //PaÅ¾ymÄ—jus objectÄ… su ctrl arba paspaudus vienÄ… objetÄ…
                                {
                                    rect = new Rectangle(startPos.x - 2, startPos.y - 2, 5, 5);
                                    AffineTransform screenToWorld = mapFrame.getMapPane().getScreenToWorldTransform();
                                    worldRect = screenToWorld.createTransformedShape(rect).getBounds2D();
                                    bbox = new ReferencedEnvelope(worldRect, mapFrame.getMapContent().getCoordinateReferenceSystem());
                                    selectFeatures(bbox);
                                }
                            }
                        });
            }
        });
		
		
		btn1.addActionListener(new ActionListener() {
			
			@SuppressWarnings("unchecked")
			public void actionPerformed(ActionEvent e) {
				
				
				 //jeigu reikia multi select
				  
				JFileChooser fileChooser = new JFileChooser();
				FileFilter filter = new FileNameExtensionFilter("SHP File","shp");
				fileChooser.setFileFilter(filter);
				fileChooser.setMultiSelectionEnabled(true);
				fileChooser.showOpenDialog(null);
				File[] files = fileChooser.getSelectedFiles();
				
				
				for (int i=0; i<files.length; i++){
				
				try {
					
					FileDataStore store = FileDataStoreFinder.getDataStore(files[i]);
				 
				 
				
				
				
				/*JFileChooser fileChooser = new JFileChooser();
				FileFilter filter = new FileNameExtensionFilter("SHP File","shp");
				fileChooser.setFileFilter(filter);
				fileChooser.showOpenDialog(null);
				File file = fileChooser.getSelectedFile();
			
				if (file == null) {
					return;
				}
				
				
				FileDataStore store;
				try {
					
					store = FileDataStoreFinder.getDataStore(file);*/
					featureSource = store.getFeatureSource();
					queryTable.featureTypeCBox.addItem(featureSource.getName().toString());
					DataStores.mapData.put(featureSource.getName().toString(), featureSource);
					setGeometry();
					map = mapFrame.getMapContent();
					Style style = createDefaultStyle();
					FeatureLayer layer = new FeatureLayer(featureSource, style);
					map.addLayer(layer);
					mapFrame.setMapContent(map);
					mapFrame.repaint();
				} catch (IOException ex) {
					Logger.getLogger(SelectionLab.class.getName()).log(Level.SEVERE, null, ex);
				}
				}
				
			}
		});
		 map.addMapLayerListListener(new MapLayerListListener() {
				@Override
				public void layerRemoved(MapLayerListEvent event) {
					DataStores.mapData.remove(event.getLayer().getFeatureSource().getName().toString());
					queryTable.featureTypeCBox.removeItem(event.getLayer().getFeatureSource().getName().toString());
				}
				
				@Override
				public void layerAdded(MapLayerListEvent arg0) {}
				@Override
				public void layerChanged(MapLayerListEvent event) {
					
				}
				@Override
				public void layerMoved(MapLayerListEvent arg0) {}
				@Override
				public void layerPreDispose(MapLayerListEvent event) {}

			});
		
		
		btn2.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
            	ReferencedEnvelope envelope;
				try {
					if(!(IDs.isEmpty()))
                	{
						getSelectedEnvelope(IDs);
                	}
					
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			
			}
         
        });
		
		btn3.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
            	for (int i = 0; i < mapFrame.getMapContent().layers().size(); i++)
                {
                    if (mapFrame.getMapContent().layers().get(i).isSelected())
                    {
           
                        try {
							queryTable.allFeatures();
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
                        
                        queryTable.setVisible(true);  
                    }
                    
                }
       
                   
            }
        });
		
		// ----------------------------- Antra užduotis
		// 4 5 6 mygtukai
		//Keliai
		btn4.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
            
            	try {
            		RoadCalculation();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            }
        });
		//Plotai
		btn5.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
            
            	try {
            		AreaCalculation();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            }
        });
		
		
		//Pastatai
		btn6.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
            
            	try {
            		calculatePASTATAI();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            }
        });
		
		
		
		// 3 užduotis
		btn7.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
           
            	//GardenTask3 frame = new GardenTask3();
				//frame.setVisible(true);
            	GardenTask3();
				//calculations();
            }
        });
		
		
		mapFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		mapFrame.setSize(800, 600);
		mapFrame.setVisible(true);
		
		queryTable = new QueryTable();
		

	}
	
	void selectFeatures(ReferencedEnvelope bbox) {
		
		  filter = ff.intersects(ff.property(geometryAttributeName), ff.literal(JTS.toGeometry(bbox)));
		 SimpleFeatureIterator iter = null;
	        try
	        {
	            Boolean empty = true;
	            selectedFeatureIds.clear();
	            for (int i = 0; i < mapFrame.getMapContent().layers().size(); i++)
	            {
	                if (mapFrame.getMapContent().layers().get(i).isSelected())
	                {
	                    SimpleFeatureCollection selectedFeatures = (SimpleFeatureCollection)
	                            mapFrame.getMapContent().layers().get(i).getFeatureSource().getFeatures(filter);

	                    iter = selectedFeatures.features();
	                    HashSet<Identifier> selectedIds = getSelectedLayerFeatureSet(mapFrame.getMapContent().layers().get(i));
	                    
	                   
	                        if (iter.hasNext())
	                        {
	                            empty = false;
	                        }
	                        while (iter.hasNext())
	                        {
	                            SimpleFeature feature = iter.next();
	                            if (!IDs.contains(feature.getIdentifier()))
	                            {
	                                IDs.add(feature.getIdentifier());
	                                selectedIds.add(feature.getIdentifier());
	                               // list = new ArrayList<FeatureId>(IDs);
	                            }
	                        }
	                        iter.close();
	                }
	            }
	            
	            
	            if (empty)
	            {
	                IDs = new HashSet<FeatureId>();
	            }
	            displaySelectedFeatures(IDs);
	        } catch (Exception ex)
	        {
	            ex.printStackTrace();
	            return;
	        }
	}
	
	 private HashSet<Identifier> getSelectedLayerFeatureSet(Layer layer) {
	        HashSet<Identifier> set = selectedFeatureIds.get(layer);
	        if (set == null) {
	            set = new HashSet<>();
	            selectedFeatureIds.put(layer, set);
	        }
	        return set;
	    }

	
	public static void displaySelectedFeatures(HashSet<FeatureId> IDs) {
		Style style = null;
		
		if (IDs.isEmpty()) {
			//System.out.println("Kazkas blogai");
			style = createDefaultStyle();
			
		} else {
			style = createSelectedStyle(IDs);
		}
		
		Layer layer;
		for (int i=0; i<mapFrame.getMapContent().layers().size(); i++ )
		{
			 if (mapFrame.getMapContent().layers().get(i).isSelected())
             {
				 layer = mapFrame.getMapContent().layers().get(i);
				 ((FeatureLayer) layer).setStyle(style);
			
             }
			 mapFrame.getMapPane().repaint();
		}
		
	}
	
	
	private static Style createDefaultStyle() {
		Rule rule = createRule(LINE_COLOUR, FILL_COLOUR);
		
		FeatureTypeStyle fts = sf.createFeatureTypeStyle();
		fts.rules().add(rule);
		
		Style style = sf.createStyle();
		style.featureTypeStyles().add(fts);
		return style;
	}
	
	private static Style createSelectedStyle(Set<FeatureId> IDs) {
		Rule selectedRule = createRule(SELECTED_COLOUR, SELECTED_COLOUR);
		selectedRule.setFilter(ff.id(IDs));
		
		Rule otherRule = createRule(LINE_COLOUR, FILL_COLOUR);
		otherRule.setElseFilter(true);
		
		FeatureTypeStyle fts = sf.createFeatureTypeStyle();
		fts.rules().add(selectedRule);
		fts.rules().add(otherRule);
		
		Style style = sf.createStyle();
		style.featureTypeStyles().add(fts);
		return style;
	}
	
	
	
	private static Rule createRule(Color outlineColor, Color fillColor) {
		Symbolizer symbolizer = null;
		Fill fill = null;
		Stroke stroke = sf.createStroke(ff.literal(outlineColor), ff.literal(LINE_WIDTH));
		
		switch (geometryType) {
		case POLYGON:
			fill = sf.createFill(ff.literal(fillColor), ff.literal(OPACITY));
			symbolizer = sf.createPolygonSymbolizer(stroke, fill, geometryAttributeName);
			break;
			
		case LINE:
			symbolizer = sf.createLineSymbolizer(stroke, geometryAttributeName);
			break;
			
		case POINT:
			fill = sf.createFill(ff.literal(fillColor), ff.literal(OPACITY));
			
			Mark mark = sf.getCircleMark();
			mark.setFill(fill);
			mark.setStroke(stroke);
			
			Graphic graphic = sf.createDefaultGraphic();
			graphic.graphicalSymbols().clear();
			graphic.graphicalSymbols().add(mark);
			graphic.setSize(ff.literal(POINT_SIZE));
			
			symbolizer = sf.createPointSymbolizer(graphic, geometryAttributeName);
		}
		
		Rule rule = sf.createRule();
		rule.symbolizers().add(symbolizer);
		return rule;
	}
	
	
	private void setGeometry() {
		GeometryDescriptor geomDesc = featureSource.getSchema().getGeometryDescriptor();
		geometryAttributeName = geomDesc.getLocalName();
		
		Class<?> clazz = geomDesc.getType().getBinding();
		
		if (Polygon.class.isAssignableFrom(clazz) ||
				MultiPolygon.class.isAssignableFrom(clazz)) {
			geometryType = GeomType.POLYGON;
			
		} else if (LineString.class.isAssignableFrom(clazz) ||
				MultiLineString.class.isAssignableFrom(clazz)) {
			
			geometryType = GeomType.LINE;
			
		} else {
			geometryType = GeomType.POINT;
		}
		
	}
	


	public static void getSelectedEnvelope(HashSet<FeatureId> IDs) throws IOException {
        envelope = new ReferencedEnvelope();
       
        	 for (int i = 0; i < mapFrame.getMapContent().layers().size(); i++)
	            {
	                if (mapFrame.getMapContent().layers().get(i).isSelected())
	                {
	                	if(!(IDs.isEmpty()))
	                	{
	                		FeatureCollection features =  mapFrame.getMapContent().layers().get(i).getFeatureSource().getFeatures(ff.id(IDs));
	                		envelope.expandToInclude(features.getBounds());
	                	}else{
	                		System.out.println("Kazkas blogai 2");
	                	}
                }
            }
       
        	 if (null != envelope) {
        		 
					mapFrame.getMapPane().setDisplayArea(envelope);

				}
    }
	
	
	public static void AreaCalculation() throws IOException {
		
		//SimpleFeature feature = null;
		//Filter filter = null;
		
        SimpleFeatureSource areasSource =  DataStores.mapData.get("sven_PLO_P");
        SimpleFeatureSource region =  DataStores.mapData.get("sven_SAV_P");
/*
        //Gaunamas 
        SimpleFeatureCollection regionFeature = region.getFeatures(filter);
        
        //Vykdomas poligonu patikrinimas
        SimpleFeatureCollection intersectedCollection = new IntersectionFeatureCollection().execute( areasSource.getFeatures(),regionFeature,null,null,IntersectionFeatureCollection.IntersectionMode.INTERSECTION,false,false);
        SimpleFeatureIterator iterator = intersectedCollection.features();
        HashMap<String, FeatureInfo> regionAreaData = new HashMap<>();
       
  
                    while (iterator.hasNext()) {
                        feature = iterator.next();

                        String type;

                        switch (feature.getAttribute("sven_PLO_P_GKODAS").toString()) {
                            case "hd1":
                            	//type = "hd1";
                               // break;
                            case "hd2":
                            	//type = "hd2";
                               // break;
                            case "hd3":
                            	//type = "hd3";
                               // break;
                            case "hd4":
                            	 //type = "hd4";
                                 //break;
                            case "hd9":
                                type = "hd";
                                break;
                            case "ms0":
                                type = "Miskai ms0";
                                break;
                            case "ms4":
                                type = "Sodai ms4";
                                break;
                            case "pu0":
                                type = "Pastatai pu0";
                                break;
                            default:
                                type = "Unknown";
                        }

                        String key = feature.getAttribute("sven_SAV_P_SAV").toString() + "_" + type;
                        
                        //Jeigu hash maoe nėra elemento (savivaldybės), tada pridedama į hash map
                        if (!regionAreaData.containsKey(key)) {
                        	regionAreaData.put(key, new FeatureInfo());
                        }

                        FeatureInfo featureInfo = regionAreaData.get(key);

                        featureInfo.region = feature.getAttribute("sven_SAV_P_SAV").toString();
                        featureInfo.type = type;
                        featureInfo.frequency++;
                        featureInfo.size += ((Geometry)feature.getDefaultGeometry()).getArea()/1000000;
                        featureInfo.regionPlot = Double.parseDouble(feature.getAttribute("sven_SAV_P_PLOT").toString())/1000000;
                    }
            iterator.close();*/
        

        FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();

        SimpleFeatureType schema = areasSource.getSchema();

        Filter filter =null;

        for (int i = 0; i < mapFrame.getMapContent().layers().size(); i++)
        {
            if (mapFrame.getMapContent().layers().get(i).isSelected())
            {
            	if(!(IDs.isEmpty()))
            	{
            		filter=ff.id(IDs);
            	}else{
            		System.out.println("Kazkas blogai 2");
            	}
        }
      }
        
       

        SimpleFeatureCollection outerFeatures = region.getFeatures(filter);
        SimpleFeatureIterator iterator;
        SimpleFeature feature;

        String geometryPropertyName = schema.getGeometryDescriptor().getLocalName();
        CoordinateReferenceSystem targetCRS = schema.getGeometryDescriptor().getCoordinateReferenceSystem();

        ReferencedEnvelope bbox = new ReferencedEnvelope(outerFeatures.getBounds().getMinX(), outerFeatures.getBounds().getMaxX(),
                outerFeatures.getBounds().getMinY(), outerFeatures.getBounds().getMaxY(), targetCRS);

        filter = ff.bbox(ff.property(geometryPropertyName), bbox);

        SimpleFeatureCollection filteredAreaFeatures = areasSource.getFeatures(filter);

        SimpleFeatureCollection intersectedCollection = new IntersectionFeatureCollection().execute(filteredAreaFeatures,outerFeatures,null,null,IntersectionFeatureCollection.IntersectionMode.INTERSECTION,false,false);

        iterator = intersectedCollection.features();

        System.out.println("Dydis" + iterator.toString());
        
        HashMap<String, FeatureInfo> regionAreaData = new HashMap<>();
        int g=0;
        while (iterator.hasNext()) {
            feature = iterator.next();

            String type;
            
            switch (feature.getAttribute("sven_PLO_P_GKODAS").toString()) {
                case "hd1":
                case "hd2":
                case "hd3":
                case "hd4":
                case "hd9":
                    type = "Hidro";
                    break;
                case "ms0":
                    type = "Woods";
                    break;
                case "ms4":
                    type = "Gardens";
                    break;
                case "pu0":
                    type = "Buildings";
                    break;
                default:
                    type = "Unknown";
            }

            String key = feature.getAttribute("sven_SAV_P_SAV").toString() + "_" + type;
            g = g +1;
            if (!regionAreaData.containsKey(key)) {
            	regionAreaData.put(key, new FeatureInfo());
            }

            FeatureInfo featureInfo = regionAreaData.get(key);

            featureInfo.region = feature.getAttribute("sven_SAV_P_SAV").toString();
            featureInfo.type = type;
            featureInfo.frequency++;
            featureInfo.size += ((Geometry)feature.getDefaultGeometry()).getArea()/1000000;
            featureInfo.regionPlot = Double.parseDouble(feature.getAttribute("sven_SAV_P_PLOT").toString())/1000000;

        }
        iterator.close();
       

        String[][] data = new String[regionAreaData.size()][6];

        Iterator<String> iter = regionAreaData.keySet().iterator();
        int i = 0;
        while (iter.hasNext()) {
            String it = iter.next();

            data[i][0] = regionAreaData.get(it).region;
            data[i][1] = regionAreaData.get(it).type;
            data[i][2] = Integer.toString(regionAreaData.get(it).frequency);
            data[i][3] = Double.toString(regionAreaData.get(it).size);
            data[i][4] = Double.toString(regionAreaData.get(it).regionPlot);
            data[i][5] = Double.toString(regionAreaData.get(it).size / regionAreaData.get(it).regionPlot*100);
          
            i++;
        }

        String[] column = { "Regionas", "Tipas", "Kiekis", "Teritorijos plotas", "Administracinio vieneto plotas", "Santykis %"};

        javaTable(data, column);
    }

	
public static void RoadCalculation() throws IOException {
		
		//Filter filter = null;
		//SimpleFeature feature = null;
		
		SimpleFeatureSource roads = DataStores.mapData.get("sven_KEL_L");
		SimpleFeatureSource region = DataStores.mapData.get("sven_SAV_P");
		
	/*	      
		 for (int i = 0; i < mapFrame.getMapContent().layers().size(); i++){
		     if (mapFrame.getMapContent().layers().get(i).isSelected())
		     {
		     	if(!(IDs.isEmpty())){
		     		filter=ff.id(IDs);
		     	}else{
			  		System.out.println("Nėra paselectintas layer");
			  		System.exit(0);
		     	
		     }
		  	}
		 }
     

		//Gaunamas pažymėtos savivaldybės feature
        SimpleFeatureCollection regionFeature = region.getFeatures(filter);
        
        //Vykdomas poligonu patikrinimas
        //SimpleFeatureCollection intersectedCollection = new IntersectedFeatureCollection( areasSource.getFeatures(),outerFeatures);
        SimpleFeatureCollection intersection = new IntersectionFeatureCollection().execute( roads.getFeatures(),regionFeature,null,null,IntersectionFeatureCollection.IntersectionMode.INTERSECTION,false,false);
        SimpleFeatureIterator  iterator = intersection.features();
        Geometry geometry;*/
		
		 FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();

	        SimpleFeatureType schema = roads.getSchema();

	        Filter filter =null;

	        for (int i = 0; i < mapFrame.getMapContent().layers().size(); i++)
	        {
	            if (mapFrame.getMapContent().layers().get(i).isSelected())
	            {
	            	if(!(IDs.isEmpty()))
	            	{
	            		filter=ff.id(IDs);
	            	}else{
	            		System.out.println("Kazkas blogai 2");
	            	}
	        }
	      }
	       
	        GeometryDescriptor geomDesc = featureSource.getSchema().getGeometryDescriptor();
			geometryAttributeName = geomDesc.getLocalName();

	        SimpleFeatureCollection outerFeatures = region.getFeatures(filter);
	        SimpleFeatureIterator iterator;
	        SimpleFeature feature;

	        //String geometryPropertyName = schema.getGeometryDescriptor().getLocalName();
	        CoordinateReferenceSystem targetCRS = schema.getGeometryDescriptor().getCoordinateReferenceSystem();

	        ReferencedEnvelope bbox = new ReferencedEnvelope(outerFeatures.getBounds().getMinX(), outerFeatures.getBounds().getMaxX(),
	                outerFeatures.getBounds().getMinY(), outerFeatures.getBounds().getMaxY(), mapFrame.getMapContent().getCoordinateReferenceSystem());

	        filter = ff.bbox(ff.property(geometryAttributeName), bbox);

	        SimpleFeatureCollection filteredAreaFeatures = roads.getFeatures(filter);

	        SimpleFeatureCollection intersectedCollection = new IntersectionFeatureCollection().execute(filteredAreaFeatures,outerFeatures,null,null,IntersectionFeatureCollection.IntersectionMode.INTERSECTION,false,false);

	        iterator = intersectedCollection.features();

	        System.out.println("Dydis" + iterator.toString());
		
		
		
        HashMap<String, FeatureInfo> regionRoadsData = new HashMap<>();
		 
		 
       //Geometry geometry;
            while (iterator.hasNext()) {
                feature = iterator.next();

                Geometry   geometry = (Geometry) (feature.getDefaultGeometry());
                
                //Suieškomas savivaldybės/rajono atributas iš calculations HashMap
                String key = feature.getAttribute("sven_SAV_P_SAV").toString();

                if (!regionRoadsData.containsKey(key)) {
                	regionRoadsData.put(key, new FeatureInfo());   //Jeigu neranda pagal kokį nors pavadinimą, tada pridedamas naujas
                }
                
                FeatureInfo featureInfo = regionRoadsData.get(key);
                
                //Pridedami atributui reikalingi elementai
                featureInfo.region = feature.getAttribute("sven_SAV_P_SAV").toString();  //Savivaldybė/rajonas pavadinimas
                featureInfo.frequency++; //Keliu kiekis
                featureInfo.size += geometry.getLength()/1000; //Kelio ilgis paverčiamas į km
                featureInfo.regionPlot =  Double.parseDouble(feature.getAttribute( "sven_SAV_P_PLOT" ).toString())/1000000;  //Regiono plotas paverčiamas į km^2
                 
            }
            iterator.close();

        //Duomenų perkelimas į masyvus ir santykio apskaičiavimas
        String[][] data = new String[regionRoadsData.size()][5];
        Iterator<String> iter = regionRoadsData.keySet().iterator();
        int i = 0;
        while (iter.hasNext()) {
            String it = iter.next();
            
            data[i][0] = regionRoadsData.get(it).region;						//Savivaldybė/rajonas pavadinimas
            data[i][1] = Integer.toString(regionRoadsData.get(it).frequency);	//Keliu kiekis
            data[i][2] = Double.toString(regionRoadsData.get(it).size);		//Kelio ilgis
            data[i][3] = String.valueOf(regionRoadsData.get(it).regionPlot);  //Regiono plotas
            data[i][4] = Double.toString(regionRoadsData.get(it).size/regionRoadsData.get(it).regionPlot); //santykio apskaičiavimas

            i++;
        }
        
        //Sudaromas pavadinimų eilutė
        String[] columns = { "Regionas", "Kiekis", "Kelio ilgis", "Administracinio vieneto plotas", "Santykis"};

        javaTable(data, columns );
	}
	
	
	public static void javaTable(String[][] data, String[] columns){
		//Sukuriama lentelė
        JTable table = new JTable(data, columns);
        JFrame windows = new JFrame();
        windows.getContentPane().add(new JScrollPane(table));
        windows.setExtendedState(JMapFrame.MAXIMIZED_BOTH);
        windows.setVisible(true);
		
	}
	
	
	public static void calculatePASTATAI() throws IOException {
	     
	       // SimpleFeatureSource buildingsSource = DataStores.mapData.get("Pastatai");
	        //SimpleFeatureSource areasSource = DataStores.mapData.get("Plotai");
	        SimpleFeatureSource polygonsSource = DataStores.mapData.get("sven_SAV_P");
	        SimpleFeatureSource buildingsSource = null;
	        SimpleFeatureSource areasSource = null;
	        FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();
	        String geometryPropertyName;
	        //CoordinateReferenceSystem targetCRS;
	        
	        SimpleFeatureType schema = null;

	        Filter filter =null;
	        String district = null ;

	        for (int i = 0; i < mapFrame.getMapContent().layers().size(); i++)
	        {
	            if (mapFrame.getMapContent().layers().get(i).isSelected())
	            {
	            	if(!(IDs.isEmpty()))
	            	{
	            		filter=ff.id(IDs);
	            	}else{
	            		System.out.println("Kazkas blogai 2");
	            	}
	        }
	      }
	        System.out.println("Size: " + IDs.size());
	        String[][] data = new String[IDs.size()*4][5];
	        int ii=0;
	        String[] column = { "SAV", "GKODAS", "Pastatų plotas", "Teritorijos plotas", "Santykis"};
	        
	        Object[] myArr = IDs.toArray();
	        
	        for(int i=0 ; i<myArr.length; i++){
	        	System.out.println(" "+ IDs.iterator().next().toString());
	        }
	           	
	        
	        SimpleFeatureCollection outerFeatures = polygonsSource.getFeatures(filter);
	        SimpleFeatureIterator iterator;
	        SimpleFeature feature;
	        
	        String sloksnis = null;
	        SimpleFeatureIterator iterator11 = outerFeatures.features();
	        while (iterator11.hasNext()) {
	        	feature = iterator11.next();
	        	/*System.out.println(feature.getAttribute(0).toString());
	        	System.out.println(feature.getAttribute(1).toString());
	        	System.out.println(feature.getAttribute(2).toString());
	        	System.out.println(feature.getAttribute(3).toString());
	        	System.out.println(feature.getAttribute(4).toString());*/
	        	String iii =feature.getAttribute(2).toString();
	            switch(iii){
	            
	            case "JONAVOS R.SAV.":
	            	sloksnis = "Plotai";
	            	buildingsSource = DataStores.mapData.get("Pastatai");
	     	       	areasSource = DataStores.mapData.get("Plotai");
	     	       	schema = areasSource.getSchema();
	            	break;
	            case "UKMERGËS R.SAV.":
	            	sloksnis = "Plotai2";
	            	buildingsSource = DataStores.mapData.get("Pastatai22");
	     	       	areasSource = DataStores.mapData.get("Plotai2");
	     	       	schema = areasSource.getSchema();
	            	break;
	            }
	        
	        geometryPropertyName = schema.getGeometryDescriptor().getLocalName();
	        CoordinateReferenceSystem targetCRS = schema.getGeometryDescriptor().getCoordinateReferenceSystem();

	        ReferencedEnvelope bbox = new ReferencedEnvelope(outerFeatures.getBounds().getMinX(), outerFeatures.getBounds().getMaxX(),
	                outerFeatures.getBounds().getMinY(), outerFeatures.getBounds().getMaxY(), targetCRS);

	        filter = ff.bbox(ff.property(geometryPropertyName), bbox);

	        SimpleFeatureCollection filteredAreaFeatures = areasSource.getFeatures(filter);

	        IntersectionFeatureCollection intersection = new IntersectionFeatureCollection();

	        SimpleFeatureCollection intersectedCollection = intersection.execute(filteredAreaFeatures,outerFeatures,null,null,IntersectionFeatureCollection.IntersectionMode.INTERSECTION,false,false);

	        iterator = intersectedCollection.features();
	        
	        HashMap<String, Double> PLOTAIareas = new HashMap<>();
	        System.out.println("Dydis" + iterator.toString());
	        int g=0;
	        while (iterator.hasNext()) {
	            feature = iterator.next();
	            
	            String type;
	           // System.out.println(" "+feature.getAttribute("sven_PLO_P_GKODAS").toString());
	            switch (feature.getAttribute(sloksnis+"_GKODAS").toString()) {
	                case "hd1":
	                case "hd2":
	                case "hd3":
	                case "hd4":
	                case "hd9":
	                    type = "Hidro";
	                    break;
	                case "ms0":
	                    type = "Woods";
	                    break;
	                case "ms4":
	                    type = "Gardens";
	                    break;
	                case "pu0":
	                    type = "Buildings";
	                    break;
	                default:
	                    type = "Unknown";
	            }

	            String key = type;
	           
	            //String key = type;
	            g = g +1;
	            if (!PLOTAIareas.containsKey(key)) {
	            	district = feature.getAttribute("sven_SAV_P_SAV").toString();
	                PLOTAIareas.put(key, ((Geometry)feature.getDefaultGeometry()).getArea());
	            } else {
	                PLOTAIareas.put(
	                    key,
	                    PLOTAIareas.get(key) + ((Geometry)feature.getDefaultGeometry()).getArea()
	                );
	            }

	        }
	        System.out.println("Dydis g "+ g);

	        geometryPropertyName = buildingsSource.getSchema().getGeometryDescriptor().getLocalName();
	        targetCRS = buildingsSource.getSchema().getGeometryDescriptor().getCoordinateReferenceSystem();

	        bbox = new ReferencedEnvelope(outerFeatures.getBounds().getMinX(), outerFeatures.getBounds().getMaxX(),
	                outerFeatures.getBounds().getMinY(), outerFeatures.getBounds().getMaxY(), targetCRS);

	        filter = ff.bbox(ff.property(geometryPropertyName), bbox);

	        SimpleFeatureCollection filteredBuildingsFeatures = buildingsSource.getFeatures(filter);
	 
	        SimpleFeatureCollection intersectedCollection2 = new IntersectionFeatureCollection().execute(filteredBuildingsFeatures,filteredAreaFeatures, null,null,IntersectionFeatureCollection.IntersectionMode.INTERSECTION,false,false);

	        iterator = intersectedCollection2.features();
	        HashMap<String, FeatureInfo> calculations = new HashMap<>();

	        String type;

	        try {
	            while (iterator.hasNext()) {
	                feature = iterator.next();
	                /*System.out.println(j);
	                j=j+1;*/
	               /*
	                System.out.println(feature.getAttribute(1).toString());
	                System.out.println(feature.getAttribute(2).toString());
	                System.out.println(feature.getAttribute(3).toString());
	                System.out.println(feature.getAttribute(4).toString());
	                System.out.println(feature.getAttribute(5).toString());
	                System.out.println(feature.getAttribute(6).toString());
	                System.out.println(feature.getAttribute(7).toString());*/

	                switch (feature.getAttribute(7).toString()) {
	                    case "hd1":
	                    case "hd2":
	                    case "hd3":
	                    case "hd4":
	                    case "hd9":
	                        type = "Hidro";
	                        break;
	                    case "ms0":
	                        type = "Woods";
	                        break;
	                    case "ms4":
	                        type = "Gardens";
	                        break;
	                    case "pu0":
	                        type = "Buildings";
	                        break;
	                    default:
	                        type = "Unknown";
	                }

	                String key =  type;

	                if (!calculations.containsKey(key)) {
	                    calculations.put(key, new FeatureInfo());
	                }

	                FeatureInfo featureInfo = calculations.get(key);

	                featureInfo.region =  district;

	                featureInfo.type = type;
	                //featureInfo.frequency++;
	                featureInfo.size += ((Geometry) feature.getAttribute(0)).getArea()/1000000;
	                featureInfo.regionPlot = PLOTAIareas.get(key)/1000000;
	               
	            }
	        } catch (Exception e) {
	            e.printStackTrace();
	        } finally {
	            iterator.close();
	        }
	        PLOTAIareas.clear();

	        //String[][] data = new String[calculations.size()][5];

	        Iterator<String> iter = calculations.keySet().iterator();
	       
	        while (iter.hasNext()) {
	            String it = iter.next();

	            data[ii][0] = calculations.get(it).region;
	            data[ii][1] = calculations.get(it).type;
	            data[ii][2] = Double.toString(calculations.get(it).size);
	            data[ii][3] = Double.toString(calculations.get(it).regionPlot);
	            data[ii][4] = Double.toString((calculations.get(it).size * 100) / calculations.get(it).regionPlot);
	            ii++;
	        }
	        }
	        javaTable(data, column);

	    }
	 
////////////////////////////////////////////////////////////////////////////////////////////////
	 SimpleFeatureSource VietovesBuffer;
	 SimpleFeatureSource PlotaiBuffer;
	 
	 public void calculations(double atstumasNuoGyvenvietes2, double laukoIlgis2, double laukoPlotis2, double upesPlotas2, double atstumasNuoEzero2){
		 	System.out.println("Vykdomas skaičiavimas");
		 	SimpleFeatureCollection vietovesFeature = null;
		 	SimpleFeatureCollection plotaiFeature = null;
		 	SimpleFeatureCollection hidroFeature = null;
		 	SimpleFeatureCollection keliaiFeature = null;
		 	
			Filter vietovesFilter = null;
			Filter plotaiFilter = null;
			Filter hidroFilter = null;
			Filter keliaiFilter = null;
			
			SimpleFeatureSource vietovesSource =  DataStores.mapData.get("VIETOV_U");
	        SimpleFeatureSource plotaiSource =  DataStores.mapData.get("PLOTAI");
	        SimpleFeatureSource hidroSource =  DataStores.mapData.get("HIDRO_L");
	        SimpleFeatureSource keliaiSource =  DataStores.mapData.get("KELIAI");
	        
	       
			//Layer layer = ieskotiLayer("VIETOV_U");
	        //System.out.println(layer.getFeatureSource().getName().toString());
	        vietovesFilter = getLayerFeatures(ieskotiLayer("VIETOV_U"));
	        plotaiFilter = getLayerFeatures(ieskotiLayer("PLOTAI"));
	        hidroFilter = getLayerFeatures(ieskotiLayer("HIDRO_L"));
	        keliaiFilter = getLayerFeatures(ieskotiLayer("KELIAI"));
	        
	        try {
				vietovesFeature = vietovesSource.getFeatures(vietovesFilter);
				plotaiFeature = plotaiSource.getFeatures(plotaiFilter);
			    hidroFeature = hidroSource.getFeatures(hidroFilter);
			    keliaiFeature = keliaiSource.getFeatures(keliaiFilter);
			} catch (IOException e) {
				e.printStackTrace();
			}
	     
	        
			System.out.println("vietovesFeature selected "+ vietovesFeature.size());
			System.out.println("plotaiFeature selected "+ plotaiFeature.size());
			System.out.println("hidroFeature selected "+ hidroFeature.size());
			System.out.println("keliaiFeature selected "+ keliaiFeature.size());
			
			
			//testing();
			bufferVietoves(vietovesFeature, atstumasNuoGyvenvietes2);
			bufferPlotai(plotaiFeature, atstumasNuoEzero2);
			//crateUnionLayer();
			layerSkirtumas(plotaiSource);
			
			//sankirta(hidroFeature, "DeletedArea", "HidroIntersection" );
			gridMaster();
			
			SimpleFeatureSource gridSource;
			gridSource =  DataStores.mapData.get("grid");
			SimpleFeatureSource deleteAreaSource =  DataStores.mapData.get("DeletedArea");
			SimpleFeatureCollection gridFeature = null;
			SimpleFeatureCollection deleteAreaFeature = null;
			try {
				gridFeature = gridSource.getFeatures();
				deleteAreaFeature = deleteAreaSource.getFeatures();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			sankirta(hidroFeature, deleteAreaFeature, "HidroIntersection" );
			sankirta(keliaiFeature, deleteAreaFeature, "KeliaiIntersection" );
			sankirta(gridFeature, deleteAreaFeature, "SankirtaGrid");
			gridSource =  DataStores.mapData.get("SankirtaGrid");
			try {
				gridFeature=gridSource.getFeatures();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			sankirtaGridKeliai(keliaiFeature , gridFeature, "SankirtaKeliaiGrid");
			sankirtaGridHidro(hidroFeature, gridFeature , "SankirtaHidroGrid");
			//filtravimasPlotu(keliaiFeature, gridFeature);
			//dissolveKeliai();
			dissolveHidro();
			SimpleFeatureSource hidroSourceAA =  DataStores.mapData.get("HidroIntersection");
			SimpleFeatureSource keliaiSourceAA =  DataStores.mapData.get("KeliaiIntersection");
			deleteAreaSourceAll = deleteAreaSource;
			keliaiTempSource = keliaiSourceAA;
			hidroTempSource = hidroSourceAA;
			testingMethod(laukoIlgis2, laukoPlotis2, upesPlotas2);
			gridSource =  DataStores.mapData.get("Stac");
			try {
				gridFeature=gridSource.getFeatures();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			sankirta(hidroFeature,gridFeature, "ATS");
			
		}
	 
	

	private void testingMethod(double laukoIlgis2, double laukoPlotis2, double upesPlotas2) {
		double ilgis = laukoIlgis2;
		double plotis = laukoPlotis2;
		double kaskiek = 50;
		//SimpleFeatureSource hidroSource =  DataStores.mapData.get("HidroIntersection");
		SimpleFeatureSource keliaiSource =  DataStores.mapData.get("KeliaiIntersection");
		SimpleFeatureSource deleteAreaSource =  DataStores.mapData.get("DeletedArea");
		
		SimpleFeatureCollection keliaiFeature = null;
		SimpleFeatureCollection deleteAreaFeature = null;
		//SimpleFeatureCollection hidroFeature = null;
		try {
			keliaiFeature = keliaiSource.getFeatures();
			//hidroFeature = hidroSource.getFeatures();
			deleteAreaFeature = deleteAreaSource.getFeatures();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 SimpleFeatureIterator  iteratorKeliai = keliaiFeature.features();
		//SimpleFeatureIterator  iteratorGrid = gridFeature.features();
		 SimpleFeature featureKeliai;
		 SimpleFeatureType featureType = deleteAreaFeature.getSchema();
		 List<SimpleFeature> area = new ArrayList<>();
		 int ii=0;
		 SimpleFeatureType schema = deleteAreaSource.getSchema();
		 CoordinateReferenceSystem targetCRS = schema.getCoordinateReferenceSystem();
		 double si = 0;
		 while (iteratorKeliai.hasNext()) {
			 featureKeliai = iteratorKeliai.next();
		   for(int gg = 0; gg<2; gg++){	 
			 Geometry   geometry = (Geometry) (featureKeliai.getDefaultGeometry());
			 Coordinate[] coords = geometry.getCoordinates();
			 SimpleFeature changedFeature = null;
			 double xFirst = 0;
			 double yFirst = 0;
			 double xLast = 0;
			 double yLast = 0;
			 double bendrasPitagoras = 0;
			 double xx1 = 0;
			 double yy1 = 0;
			 double angle = 0;
			 boolean yes =false;
			 int keitimai = 0;
			 si = si+1;
			 System.out.println("------------------------------------KEl"+ keliaiFeature.size() + " of "+ si );
			 if(coords[0].x<coords[1].x){
				 keitimai = 0;
			 }else{
				 keitimai = 1;
			 }
			 
			 
			 for(int jj=1; jj<coords.length; jj++) {
				 if(keitimai ==0){
				if(!((xx1 > coords[coords.length-1].x))){
					
				}else{
					 jj = coords.length;
					 break;
				 }
				 }else{
				if(!(xx1 < coords[coords.length-1].x)){
					
				}else{
					 jj = coords.length;
					 break;
				 }
				 }	 
						 
				 if(jj==1){
					 
					 if(yes == true){
					 if(keitimai ==0){
						 while(coords[jj-1].x < xx1){
							 jj = jj+1;
						 }
					 }else{
						 //System.out.println("a "+xx1);
						 while(coords[jj-1].x > xx1){
							 //System.out.println(coords[jj-1].x);
							 jj = jj+1;
						 }
					 }
					 }
					 if(yes == false){
						 System.out.println("Pradidinis elementas");
						 xx1 = coords[jj-1].x;
						 yy1 = coords[jj-1].y;
						 System.out.println(coords[jj-1].x +" "+ coords[jj-1].y );
					 }	 
				 }
				
				    GeodeticCalculator gc = new GeodeticCalculator(targetCRS);
				    if(yes == true){
				    	System.out.println("Daromas slenkimas");
				    	try {
				    		System.out.println("xx1, yy1 "+ xx1+" " +" "+ yy1+" ");
							gc.setStartingPosition( JTS.toDirectPosition( new Coordinate(xx1, yy1), targetCRS)  );
							gc.setDestinationPosition( JTS.toDirectPosition( coords[jj-1], targetCRS)  );
						} catch (TransformException e) {
							e.printStackTrace();
						}
				    	yes = false;
				    }else{
				    	System.out.println("Daromas pirmas variant");
					    try {
							gc.setStartingPosition( JTS.toDirectPosition( coords[jj-1], targetCRS)  );
							gc.setDestinationPosition( JTS.toDirectPosition( coords[jj], targetCRS)  );
						} catch (TransformException e) {
							e.printStackTrace();
						}
				    }
				 
				    
				    double distance = gc.getOrthodromicDistance();
				    //ArrayList<Double> angles = new ArrayList<Double>();
				    angle = gc.getAzimuth();
				    

				 bendrasPitagoras = bendrasPitagoras+ distance;
				 if(bendrasPitagoras >= ilgis){
					
					 double blogojiDalis = bendrasPitagoras -ilgis;
					 double gerojiDalis = distance - blogojiDalis;
					 System.out.println(ilgis);
					 System.out.println(blogojiDalis);
					 System.out.println(gerojiDalis);
					 System.out.println(distance);
					 
					 	double a = 0.0;
					    if(((angle >= -180.0) && (angle <= -90.0)) ||((angle <= 180.0) && (angle >= 90.0))){
					    	a = Math.sin(angle-180)*gerojiDalis;	
					    }else{
					    	a = Math.sin(angle)*gerojiDalis;
					    }
					    double xxx;
					    double yyy;
					    double b = Math.sqrt(gerojiDalis*gerojiDalis-a*a);
					    if(keitimai ==0){
					    	xxx = coords[jj-1].x+b;
					    	yyy = coords[jj-1].y+a;
					    	System.out.println("X "+xxx +" Y "+yyy);
					    }else{
					    	xxx = coords[jj-1].x-b;
					    	yyy = coords[jj-1].y-a;
					    }
						 xFirst= xx1;
						 yFirst= yy1;
						 xLast= xxx;
						 yLast= yyy;
						 
						 GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory( null );

						 WKTReader reader = new WKTReader( geometryFactory );
						 Polygon polygon = null;
						 try {
							 if(keitimai ==0){	 
								 if(gg == 0){
									 polygon = (Polygon) reader.read("POLYGON(("+xFirst+" "+yFirst+","+(xLast)+" "+yLast+","+(xLast)+" "+(yLast+plotis)+","+(xFirst)+" "+(yFirst+plotis)+","+xFirst+" "+yFirst+"))");
								 }else{
									 polygon = (Polygon) reader.read("POLYGON(("+xFirst+" "+yFirst+","+(xLast)+" "+yLast+","+(xLast)+" "+(yLast-plotis)+","+(xFirst)+" "+(yFirst-plotis)+","+xFirst+" "+yFirst+"))");
								 }
							 }else{
									 if(gg == 0){
										 polygon = (Polygon) reader.read("POLYGON(("+xFirst+" "+yFirst+","+(xLast)+" "+yLast+","+(xLast)+" "+(yLast-plotis)+","+(xFirst)+" "+(yFirst-plotis)+","+xFirst+" "+yFirst+"))");
									 }else{
										 polygon = (Polygon) reader.read("POLYGON(("+xFirst+" "+yFirst+","+(xLast)+" "+yLast+","+(xLast)+" "+(yLast+plotis)+","+(xFirst)+" "+(yFirst+plotis)+","+xFirst+" "+yFirst+"))");
									 } 
									 
								 }
							//polygon = (Polygon) reader.read("POLYGON(("+xFirst+" "+yFirst+","+(xLast)+" "+yLast+","+(xLast)+" "+(yLast+500)+","+(xFirst)+" "+(yFirst+500)+","+xFirst+" "+yFirst+"))");
							//polygon = (Polygon) reader.read("POLYGON(("+xFirst+" "+yFirst+","+(xLast)+" "+yLast+","+(xLast)+" "+(yLast-500)+","+(xFirst)+" "+(yFirst-500)+","+xFirst+" "+yFirst+"))");
						} catch (ParseException e) {
							
							e.printStackTrace();
						}
						 for(int j=0; j<1; j++){
							 SimpleFeatureBuilder builder = new SimpleFeatureBuilder(featureType);
							 builder.add(polygon);
							 ii = ii+1;
							 changedFeature =builder.buildFeature(" "+ii+" ");
							 List<SimpleFeature> set = new ArrayList<>();
							 set.add(changedFeature);
							 SimpleFeatureSource  setSource = DataUtilities.source(new ListFeatureCollection(featureType, set));
							 Geometry firstFeatureGeometry = (Geometry) changedFeature.getDefaultGeometry();
							 firstFeatureGeometry = firstFeatureGeometry.buffer(0.5);
							 if (dissolveHidro2(setSource, upesPlotas2) && (sutvarkykViską(firstFeatureGeometry))){
								 System.out.println("Vykdomas geras");
									 //sankirtaKelHidro();
									 xx1=xLast;
									 yy1 =yLast;
									 area.add(changedFeature);
									 jj=1;
									 yes = true;
									 bendrasPitagoras = 0;
							 }else{
								    System.out.println("Vykdomas sitas");
								 	double aa = 0.0;
								    if(((angle >= -180.0) && (angle <= -90.0)) ||((angle <= 180.0) && (angle >= 90.0))){
								    	System.out.println("Vykdomas aa -180");
								    	aa = Math.sin(angle-180)*kaskiek;	
								    }else{
								    	System.out.println("Vykdomas aa ");
								    	aa = Math.sin(angle)*kaskiek;
								    }
								    double bb = Math.sqrt(kaskiek*kaskiek-aa*aa);
								    double xxx1;
								    double yyy1;
								    if(keitimai ==0){
								    	 xxx1 = xx1+bb;
								    	 yyy1 = yy1+aa;
								    }else{
								    	 xxx1 = xx1-bb;
								    	 yyy1 = yy1-aa;
								    }
								    System.out.println("aa "+aa +" bb "+bb+ "Math.sin(angle-180) "+ Math.sin(angle-180)+ "Math.sin(angle) "+ Math.sin(angle) + " angle "+ angle);
								    System.out.println("XX "+xxx1 +" YY "+yyy1);
								    xx1 = xxx1;
								    yy1 = yyy1;
								    jj=1;
								    yes = true;
								    bendrasPitagoras = 0;
								 
							 }
						 	}	 
				 		}
			 }
		 }
		 }
		 SimpleFeatureSource  resultSource = DataUtilities.source(new ListFeatureCollection(featureType, area));
		 paintLayer(resultSource, "Stac");
		 paintLayer(deleteAreaSourceAll, "Tuscios_vietos");
		
	}
	
	
	
	
	
	private void sankirtaKelHidro() {

		SimpleFeatureCollection Feature = null;
		SimpleFeatureCollection deletedFeature = null;
		for (int i = 0; i < 2; i++) {
			switch (i) {
			case 0:
				try {
					Feature = keliaiTempSource.getFeatures();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				break;
			case 1:
				try {
					Feature = hidroTempSource.getFeatures();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				break;
			}

			try {
				deletedFeature = deleteAreaSourceAll.getFeatures();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SimpleFeatureCollection intersectedCollection2 = new IntersectionFeatureCollection()
					.execute(
							Feature,
							deletedFeature,
							null,
							null,
							IntersectionFeatureCollection.IntersectionMode.INTERSECTION,
							false, false);
			SimpleFeatureIterator iteratorVietoves;

			SimpleFeatureType featureType = intersectedCollection2.getSchema();
			iteratorVietoves = intersectedCollection2.features();

			SimpleFeature feature;
			List<SimpleFeature> area = new ArrayList<>();
			SimpleFeatureBuilder builder = new SimpleFeatureBuilder(featureType);

			while (iteratorVietoves.hasNext()) {
				feature = iteratorVietoves.next();
				SimpleFeature changedFeature = null;
				for (AttributeDescriptor att : featureType
						.getAttributeDescriptors()) {
					Object value = feature.getAttribute(att.getName());
					builder.set(att.getName(), value);
				}
				changedFeature = builder.buildFeature(feature.getID());
				area.add(changedFeature);
			}
			iteratorVietoves.close();

			SimpleFeatureSource resultSource = DataUtilities
					.source(new ListFeatureCollection(featureType, area));
			switch (i) {
			case 0:
				keliaiTempSource = resultSource;
				break;
			case 1:
				hidroTempSource = resultSource;
				break;
			}
		}
	}

	private boolean sutvarkykViską(Geometry firstFeatureGeometry) {
		
		SimpleFeatureCollection deleteAreaFeatureAll = null;
		try {
			deleteAreaFeatureAll = deleteAreaSourceAll.getFeatures();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SimpleFeatureType schema = deleteAreaFeatureAll.getSchema();
		SimpleFeatureIterator  area = deleteAreaFeatureAll.features();
		Geometry removedArea = null;
		while(area.hasNext()){
			SimpleFeature simpleFeature = area.next();
			Geometry   geometry = (Geometry) (simpleFeature.getDefaultGeometry());
			if(geometry.contains( firstFeatureGeometry )){
				System.out.println("Yra");
				removedArea = geometry.difference(firstFeatureGeometry);
				break;
			}
		}
		
		if(removedArea != null){
			List<SimpleFeature> areaList = new ArrayList<>();
		 	SimpleFeatureBuilder builder = new SimpleFeatureBuilder(schema);
		 	builder.add(removedArea);
		 	SimpleFeature changedFeature = builder.buildFeature("1");
		 	areaList.add(changedFeature);
		 	SimpleFeatureSource  resultSource = DataUtilities.source(new ListFeatureCollection(schema, areaList));
		 	deleteAreaSourceAll= resultSource;
		 	return true;
		}else{
			return false;
		}
	}

	//Gerai padarytas
	private boolean dissolveHidro2(SimpleFeatureSource setSource, double upesPlotas2) {
		SimpleFeatureSource gridSource =  DataStores.mapData.get("HidroIntersection");
		 SimpleFeatureCollection hidroFeature = null;
		 SimpleFeatureCollection plotas = null;
		
		 try {
			 	hidroFeature = gridSource.getFeatures();
			 	plotas = setSource.getFeatures();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 hidroFeature = new IntersectionFeatureCollection().execute(hidroFeature,plotas, null,null,IntersectionFeatureCollection.IntersectionMode.INTERSECTION,false,false);
		 
		 SimpleFeatureIterator  iterator = hidroFeature.features();
	   
	        SimpleFeature feature;
	        double bendraUpesPlotas = 0;
	        while (iterator.hasNext()) {
	            feature = iterator.next();
	       
               double upesIlgis = Double.parseDouble(feature.getAttribute(8).toString());
              
				 if(upesIlgis == 0.0){
					 upesIlgis = 0.1;
				 }
				 
				 double upe = upesIlgis* ((Geometry)feature.getDefaultGeometry()).getLength();
				 //System.out.println("Plotas: "+ upe); 
				 bendraUpesPlotas = bendraUpesPlotas + upe;
      
	        }
	        iterator.close();
	        
	        //System.out.println("bendraUpesPlotas"+ bendraUpesPlotas);
		    Double upesPlotas = upesPlotas2;
		   
	           if( upesPlotas <= bendraUpesPlotas){
	        	   return true;
	        	   
			        }else{
			        	return false;
			        }
	      
	
	}

	private void dissolveHidro() {
		 SimpleFeatureSource gridSource =  DataStores.mapData.get("SankirtaHidroGrid");
		 SimpleFeatureSource gridGridSource =  DataStores.mapData.get("SankirtaGrid");
		 SimpleFeatureCollection hidroFeature = null;
		
		 
		 try {
			 	hidroFeature = gridSource.getFeatures();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 
		 SimpleFeatureIterator  iterator = hidroFeature.features();
	        
	        HashMap<String, FeatureInfo> PLOTAIareas = new HashMap<>();
	        System.out.println("Dydis" + iterator.toString());
	        SimpleFeature feature;
	        
	        while (iterator.hasNext()) {
	            feature = iterator.next();
	            
	            String key = feature.getAttribute(10).toString();
	            
	            if (!PLOTAIareas.containsKey(key)) {
	            	PLOTAIareas.put(key, new FeatureInfo());
                }

                FeatureInfo featureInfo = PLOTAIareas.get(key);
                double upesIlgis = Double.parseDouble(feature.getAttribute(8).toString());
               
				 if(upesIlgis == 0.0){
					 upesIlgis = 0.1;
				 }
				 double upe = upesIlgis* ((Geometry)feature.getDefaultGeometry()).getLength();
				 //System.out.println("Plotas: "+ upe); 
                featureInfo.regionPlot += upe;
	        }
	        iterator.close();
	        
	        Iterator<String> iter = PLOTAIareas.keySet().iterator();
		    Double upesPlotas = 0.0;
		    List<SimpleFeature> area = new ArrayList<>();
		    SimpleFeatureType featureType = null;
	        while (iter.hasNext()) {
	            String it = iter.next();
	            Double sk =PLOTAIareas.get(it).regionPlot;
	           if( upesPlotas <= sk){
	        	   SimpleFeatureCollection ats = null;
	        	   try {
	        		   //System.out.println("Radome");
	        		    ats=  gridGridSource.getFeatures(CQL.toFilter("grid_id = '" + it + "'"));
				} catch (CQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        	   
	        	    featureType = gridGridSource.getSchema();
				 	SimpleFeatureIterator iteratorVietoves = ats.features();
				 	SimpleFeature  featuree;
			        SimpleFeatureBuilder builder = new SimpleFeatureBuilder(featureType);
			        while (iteratorVietoves.hasNext()) {
						 featuree = iteratorVietoves.next();
						 
						 for (AttributeDescriptor att : featureType.getAttributeDescriptors()) {
					            Object value = featuree.getAttribute( att.getName() );
					            builder.set(att.getName(), value);
					      }
						 SimpleFeature changedFeature = null;
						 changedFeature =builder.buildFeature(featuree.getID());
				          area.add(changedFeature);
			        }
	           }
	           
	        }
	       
	        SimpleFeatureSource  resultSource = DataUtilities.source(new ListFeatureCollection(featureType, area));
			paintLayer(resultSource, "PlotaiIsmesti");
		 
		
	}

	private void sankirtaGridKeliai(SimpleFeatureCollection gridFeature, SimpleFeatureCollection AreaFeature, String naujas) {
		 //SimpleFeatureSource features =  DataStores.mapData.get(gridFeature);
		 //SimpleFeatureSource deletedSource =  DataStores.mapData.get(AreaFeature);
		 SimpleFeatureCollection deletedFeature = null;
		 SimpleFeatureCollection Feature = null;
		
		//deletedFeature = deletedSource.getFeatures();
		//Feature = features.getFeatures();
		deletedFeature= AreaFeature;
		Feature =gridFeature;
		 SimpleFeatureCollection intersectedCollection2 = new IntersectionFeatureCollection().execute(Feature,deletedFeature, null,null,IntersectionFeatureCollection.IntersectionMode.INTERSECTION,false,false);
		 
		// SimpleFeatureIterator iteratorVietoves;
			
		 	SimpleFeatureType featureType = intersectedCollection2.getSchema();
		 	SimpleFeatureIterator iteratorVietoves = intersectedCollection2.features();
			
			SimpleFeature  feature;
			//Geometry firstFeatureGeometry;
	        List<SimpleFeature> area = new ArrayList<>();
	        SimpleFeatureBuilder builder = new SimpleFeatureBuilder(featureType);
	        
			while (iteratorVietoves.hasNext()) {
				 feature = iteratorVietoves.next();
				 //firstFeatureGeometry = (Geometry) feature.getDefaultGeometry();
				 SimpleFeature changedFeature = null;
				 for (AttributeDescriptor att : featureType.getAttributeDescriptors()) {
			            Object value = feature.getAttribute( att.getName() );
			            if(att.getName().toString().equals("KELIAI_Shape_Leng")){
			            	builder.set(att.getName(), ((Geometry)feature.getDefaultGeometry()).getLength());
			            	
			            }else{
			            	builder.set(att.getName(), value);
			            }
			            
			      }
				  //builder.add(((Geometry)feature.getDefaultGeometry()).getLength() );
		          changedFeature =builder.buildFeature(feature.getID());
		          area.add(changedFeature);
		            					
			}
			iteratorVietoves.close();
			
			SimpleFeatureSource  resultSource = DataUtilities.source(new ListFeatureCollection(featureType, area));
			paintLayer(resultSource, naujas);
	
	}
	
	

	private void sankirtaGridHidro(SimpleFeatureCollection gridFeature, SimpleFeatureCollection AreaFeature, String naujas) {
		 //SimpleFeatureSource features =  DataStores.mapData.get(gridFeature);
		 //SimpleFeatureSource deletedSource =  DataStores.mapData.get(AreaFeature);
		 SimpleFeatureCollection deletedFeature = null;
		 SimpleFeatureCollection Feature = null;
		
		//deletedFeature = deletedSource.getFeatures();
		//Feature = features.getFeatures();
		deletedFeature= AreaFeature;
		Feature =gridFeature;
		 SimpleFeatureCollection intersectedCollection2 = new IntersectionFeatureCollection().execute(Feature,deletedFeature, null,null,IntersectionFeatureCollection.IntersectionMode.INTERSECTION,false,false);
		 
		 SimpleFeatureIterator iteratorVietoves;
			
		 	SimpleFeatureType featureType = intersectedCollection2.getSchema();
			iteratorVietoves = intersectedCollection2.features();
			
			SimpleFeature  feature;
			//Geometry firstFeatureGeometry;
	        List<SimpleFeature> area = new ArrayList<>();
	        SimpleFeatureBuilder builder = new SimpleFeatureBuilder(featureType);
	        
			while (iteratorVietoves.hasNext()) {
				 feature = iteratorVietoves.next();
				 //firstFeatureGeometry = (Geometry) feature.getDefaultGeometry();
				 SimpleFeature changedFeature = null;
				 
		       
				 for (AttributeDescriptor att : featureType.getAttributeDescriptors()) {
			            Object value = feature.getAttribute( att.getName() );
			            if(att.getName().toString().equals("HIDRO_L_Shape_Leng")){
			            	builder.set(att.getName(), ((Geometry)feature.getDefaultGeometry()).getLength());
			            	
			            }else{
			            	builder.set(att.getName(), value);
			            }
			            
			      }
			 
		         /* builder.add(((Geometry)feature.getDefaultGeometry()).getLength());
		          builder.add(feature.getAttribute(8));*/
		          
		          changedFeature =builder.buildFeature(feature.getID());
		          area.add(changedFeature);
		            					
			}
			iteratorVietoves.close();
			
			SimpleFeatureSource  resultSource = DataUtilities.source(new ListFeatureCollection(featureType, area));
			paintLayer(resultSource, naujas);
	
	}
	 
	 private void sankirta(SimpleFeatureCollection gridFeature, SimpleFeatureCollection AreaFeature, String naujas) {
		//SimpleFeatureSource features =  DataStores.mapData.get(gridFeature);
		 //SimpleFeatureSource deletedSource =  DataStores.mapData.get(AreaFeature);
		 SimpleFeatureCollection deletedFeature = null;
		 SimpleFeatureCollection Feature = null;
		
		//deletedFeature = deletedSource.getFeatures();
		//Feature = features.getFeatures();
		deletedFeature= AreaFeature;
		Feature =gridFeature;
		 SimpleFeatureCollection intersectedCollection2 = new IntersectionFeatureCollection().execute(Feature,deletedFeature, null,null,IntersectionFeatureCollection.IntersectionMode.INTERSECTION,false,false);
		 SimpleFeatureIterator iteratorVietoves;
			
		 	SimpleFeatureType featureType = intersectedCollection2.getSchema();
			iteratorVietoves = intersectedCollection2.features();
			
			SimpleFeature  feature;
			//Geometry firstFeatureGeometry;
	        List<SimpleFeature> area = new ArrayList<>();
	        SimpleFeatureBuilder builder = new SimpleFeatureBuilder(featureType);
	        
			while (iteratorVietoves.hasNext()) {
				 feature = iteratorVietoves.next();
				 //firstFeatureGeometry = (Geometry) feature.getDefaultGeometry();
				 SimpleFeature changedFeature = null;
			
				 /*double upesIlgis = Double.parseDouble(feature.getAttribute(8).toString());
				 double upesPlotis = Double.parseDouble(feature.getAttribute(9).toString());
				 if(upesIlgis == 0.0){
					 upesIlgis = 1;
				 }
				 double upe = upesIlgis* upesPlotis;
		            if((upe >= upesPlotasSalyga)){*/
		            	 for (AttributeDescriptor att : featureType.getAttributeDescriptors()) {
					            Object value = feature.getAttribute( att.getName() );
					            builder.set(att.getName(), value);
					      }
						 changedFeature =builder.buildFeature(feature.getID());
						 area.add(changedFeature);
		            	
		            //}
				
			}
			iteratorVietoves.close();
			
			SimpleFeatureSource  resultSource = DataUtilities.source(new ListFeatureCollection(featureType, area));
			paintLayer(resultSource, naujas);
	
	}

	private void sankirtaKeliai(SimpleFeatureCollection keliaiFeature, String senas, String naujas) {
		 SimpleFeatureSource deletedSource =  DataStores.mapData.get(senas);
		 SimpleFeatureCollection deletedFeature = null;
		 double ilgis = 500.0;
		 double plotis = 400.0;
		try {
			deletedFeature = deletedSource.getFeatures();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 SimpleFeatureCollection intersectedCollection2 = new IntersectionFeatureCollection().execute(keliaiFeature,deletedFeature, null,null,IntersectionFeatureCollection.IntersectionMode.INTERSECTION,false,false);
		 
		 SimpleFeatureIterator iteratorVietoves;
			
		 	SimpleFeatureType featureType = intersectedCollection2.getSchema();
			iteratorVietoves = intersectedCollection2.features();
			
			SimpleFeature  feature;
			//Geometry firstFeatureGeometry;
	        List<SimpleFeature> area = new ArrayList<>();
	        SimpleFeatureBuilder builder = new SimpleFeatureBuilder(featureType);
	        
			while (iteratorVietoves.hasNext()) {
				 feature = iteratorVietoves.next();
				 //firstFeatureGeometry = (Geometry) feature.getDefaultGeometry();
				 SimpleFeature changedFeature = null;
			
				 double kelioIlgis = Double.parseDouble(feature.getAttribute(11).toString());
		            if((kelioIlgis >= ilgis) || (kelioIlgis >= plotis)){
		            	 for (AttributeDescriptor att : featureType.getAttributeDescriptors()) {
					            Object value = feature.getAttribute( att.getName() );
					            builder.set(att.getName(), value);
					      }
						 changedFeature =builder.buildFeature(feature.getID());
						 area.add(changedFeature);
		            	
		            }
				
			}
			iteratorVietoves.close();
			
			SimpleFeatureSource  resultSource = DataUtilities.source(new ListFeatureCollection(featureType, area));
			paintLayer(resultSource, naujas);
	
		
	}
	 
	 public void paintLayer(SimpleFeatureSource resultSource, String string){
		 featureSource = resultSource;
		 queryTable.featureTypeCBox.addItem(string);
		 DataStores.mapData.put(string, featureSource);
		 setGeometry();
		 map = mapFrame.getMapContent();
		 Style style = createDefaultStyle();
		 FeatureLayer layer = new FeatureLayer(featureSource, style);
		 map.addLayer(layer);
		 mapFrame.setMapContent(map);
		 mapFrame.repaint();
		 
	 }

	private void layerSkirtumas(SimpleFeatureSource plotaiSource) {
		 GeometryFactory factory=new GeometryFactory();
		 Geometry notGeom = allUnion;
	     Geometry andGeom = factory.toGeometry(bboxBOX);
	        
	     Geometry envelopeInternal = andGeom.difference(notGeom);
	     SimpleFeatureType schema = plotaiSource.getSchema();
	     
	     List<SimpleFeature> area = new ArrayList<>();
		 SimpleFeatureBuilder builder = new SimpleFeatureBuilder(schema);
		 builder.add(envelopeInternal);
		 SimpleFeature changedFeature = builder.buildFeature("1");
		 area.add(changedFeature);
		 SimpleFeatureSource  resultSource = DataUtilities.source(new ListFeatureCollection(schema, area));
		 paintLayer(resultSource, "DeletedArea");
	        
	}

	private void crateUnionLayer() {
		 List<SimpleFeature> area = new ArrayList<>();
		 SimpleFeatureBuilder builder = new SimpleFeatureBuilder(featureType);
		 builder.add(allUnion);
		 SimpleFeature changedFeature = builder.buildFeature("1");
		 area.add(changedFeature);
		 SimpleFeatureSource  resultSource = DataUtilities.source(new ListFeatureCollection(featureType, area));
		 paintLayer(resultSource, "Union");
		
	}

	private void bufferVietoves(SimpleFeatureCollection Feature, double atstumasNuoGyvenvietes2) {
		 
		 	SimpleFeatureIterator iteratorVietoves;
			
		 	SimpleFeatureType featureType = Feature.getSchema();
			iteratorVietoves = Feature.features();
			
			SimpleFeature  feature;
			Geometry firstFeatureGeometry;
	        List<SimpleFeature> area = new ArrayList<>();
	        SimpleFeatureBuilder builder = new SimpleFeatureBuilder(featureType);
	        
			while (iteratorVietoves.hasNext()) {
				 feature = iteratorVietoves.next();
				 
				 
				 firstFeatureGeometry = (Geometry) feature.getDefaultGeometry();
				 firstFeatureGeometry = firstFeatureGeometry.buffer(atstumasNuoGyvenvietes2);
				 unionGeometry(firstFeatureGeometry, featureType);
				 SimpleFeature changedFeature = null;
				 
				 for (AttributeDescriptor att : featureType.getAttributeDescriptors()) {
			            Object value = feature.getAttribute( att.getName() );
			            builder.set(att.getName(), value);
			        }
				 //changedFeature =builder.buildFeature(feature.getID());
				 
				 builder.add(firstFeatureGeometry);
				 changedFeature =builder.buildFeature(feature.getID());
				 //System.out.println("ID VIETOVES "+feature.getID());
				 area.add(changedFeature);
				
			}
			iteratorVietoves.close();
			
			SimpleFeatureSource  resultSource = DataUtilities.source(new ListFeatureCollection(featureType, area));
			VietovesBuffer = resultSource;
			//paintLayer(resultSource, "VietovesBuffer");


	}
	 
	 private void bufferPlotai(SimpleFeatureCollection Feature, double atstumasNuoEzero2) {
		 
		 	SimpleFeatureIterator iteratorVietoves;
			
			 SimpleFeatureType featureType = Feature.getSchema();
			iteratorVietoves = Feature.features();
			
			SimpleFeature  feature;
			Geometry firstFeatureGeometry;
	        List<SimpleFeature> area = new ArrayList<>();
	        SimpleFeatureBuilder builder = new SimpleFeatureBuilder(featureType);
	        
			while (iteratorVietoves.hasNext()) {
				 feature = iteratorVietoves.next();
				 String type = feature.getAttribute(2).toString();
				//System.out.println(type);
				 //ezerai ; miskai; pramoniniu sodu masyvai; uzstatytos teritorijos
				 if(type.equals("hd3") || type.equals("ms0") || type.equals("ms4") || type.equals("pu0")){
					 
					 firstFeatureGeometry = (Geometry) feature.getDefaultGeometry();
					 firstFeatureGeometry = firstFeatureGeometry.buffer(atstumasNuoEzero2);
					 unionGeometry(firstFeatureGeometry, featureType);
					 SimpleFeature changedFeature = null;
					 
					 for (AttributeDescriptor att : featureType.getAttributeDescriptors()) {
				            Object value = feature.getAttribute( att.getName() );
				            builder.set(att.getName(), value);
				        }
					 //changedFeature =builder.buildFeature(feature.getID());
					 
					builder.add(firstFeatureGeometry);
					 changedFeature =builder.buildFeature(feature.getID());
					// System.out.println("ID PLOTAI "+feature.getID());
					 area.add(changedFeature);
				 }
			}
			iteratorVietoves.close();
			
			SimpleFeatureSource  resultSource = DataUtilities.source(new ListFeatureCollection(featureType, area));
			PlotaiBuffer = resultSource;
			//paintLayer(resultSource, "PlotaiBuffer");
		
	}

	public void unionGeometry(Geometry firstFeatureGeometry, SimpleFeatureType featureType) {
	
        if( allUnion == null ){
        	allUnion = firstFeatureGeometry;
        }
        else {
        	allUnion = allUnion.union( firstFeatureGeometry );
        }
		
	}

	public Layer ieskotiLayer(String pavadinimas){
		 for (int i = 0; i < mapFrame.getMapContent().layers().size(); i++)
         {
             if (pavadinimas.equals( mapFrame.getMapContent().layers().get(i).getFeatureSource().getName().toString()))
             {
            	 
                return mapFrame.getMapContent().layers().get(i);
             }
             
         }
		return null;
		 
	 }
	
	public void gridMaster(){
		SimpleFeatureSource ozMapSource =  DataStores.mapData.get("DeletedArea");
		// Set the grid size (1 degree) and create a bounding envelope
	    // that is neatly aligned with the grid size
	    double sideLen = 1.0;
	    ReferencedEnvelope gridBounds = null;
		try {
			gridBounds = Envelopes.expandToInclude(ozMapSource.getBounds(), sideLen);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	    // Create a feature type
	    SimpleFeatureTypeBuilder tb = new SimpleFeatureTypeBuilder();
	    tb.setName("grid");
	    tb.add(GridFeatureBuilder.DEFAULT_GEOMETRY_ATTRIBUTE_NAME,
	            Polygon.class, gridBounds.getCoordinateReferenceSystem());
	    tb.add("id", Integer.class);
	    SimpleFeatureType TYPE = tb.buildFeatureType();

	    // Build the grid the custom feature builder class
	    GridFeatureBuilder builder = new IntersectionBuilder(TYPE, ozMapSource);
	    //SimpleFeatureSource grid = Grids.createHexagonalGrid(gridBounds, sideLen, -1, builder);
	    /*try {
			gridBounds = new ReferencedEnvelope(ozMapSource.getBounds(), null);
		} catch (MismatchedDimensionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	    double width = 200.0;
	    double height = 500.0;
	    //GridFeatureBuilder builder = new DefaultGridFeatureBuilder();
	    SimpleFeatureSource grid = Oblongs.createGrid(gridBounds, width, height, builder);
	    
	    paintLayer(grid, "grid");
	    
	    
	    
	}
	 
	 public Filter getLayerFeatures(Layer layer){
		 
		Filter filter = ff.id(selectedFeatureIds.get(layer));
	        
	     return filter;
		
	 }
	
	
	 
	 private JPanel contentPane;
		private JTextField textField;
		private JTextField textField_1;
		private JLabel lblIlgis;
		private JLabel lblPlotis;
		private JTextField textField_2;
		private JLabel lblAtstumasNuoVandens;
		private JTextField textField_3;
		private JTextField textField_4;
		private JLabel lblAtstumasNuoEero;
		private JLabel lblM;
		private JLabel label;
		private JLabel label_1;
		private JLabel label_2;
		private JLabel lblUpstvekinioPlotas;
		private JTextField textField_5;
		private JLabel lblM_1;
		public static double atstumasNuoGyvenvietes = 0;
		public static double laukoIlgis = 0;
		public static double laukoPlotis = 0;
		public static double upesPlotas = 0;
		public static double atstumasNuoEzero = 0;
		
		public void GardenTask3() {
			setVisible(true);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setBounds(100, 100, 450, 300);
			contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(contentPane);
			contentPane.setLayout(null);
			
			JButton btnIekoti = new JButton("Ie\u0161koti");
			btnIekoti.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					atstumasNuoGyvenvietes = Double.parseDouble(textField_2.getText());
					laukoIlgis = Double.parseDouble(textField.getText());
					laukoPlotis = Double.parseDouble(textField_1.getText());
					upesPlotas = Double.parseDouble(textField_5.getText());
					atstumasNuoEzero = Double.parseDouble(textField_4.getText());
					//setVisible(false);
					//dispose();
					calculations(atstumasNuoGyvenvietes, laukoIlgis, laukoPlotis, upesPlotas, atstumasNuoEzero);
				}
			});
			btnIekoti.setBounds(162, 227, 89, 23);
			contentPane.add(btnIekoti);
			
			//ilgis
			textField = new JTextField();
			textField.setBounds(96, 86, 86, 20);
			contentPane.add(textField);
			textField.setColumns(10);
			
			//plotis
			textField_1 = new JTextField();
			textField_1.setBounds(279, 86, 86, 20);
			contentPane.add(textField_1);
			textField_1.setColumns(10);
			
			JLabel lblSklypoPlotas = new JLabel("Sklypo dydis");
			lblSklypoPlotas.setBounds(40, 61, 71, 14);
			contentPane.add(lblSklypoPlotas);
			
			lblIlgis = new JLabel("Ilgis:");
			lblIlgis.setBounds(40, 86, 46, 14);
			contentPane.add(lblIlgis);
			
			lblPlotis = new JLabel("Plotis:");
			lblPlotis.setBounds(225, 89, 46, 14);
			contentPane.add(lblPlotis);
			
			JLabel lblAtstumasNuoGyvenviets = new JLabel("Atstumas nuo gyvenviet\u0117s");
			lblAtstumasNuoGyvenviets.setBounds(40, 27, 151, 14);
			contentPane.add(lblAtstumasNuoGyvenviets);
			
			//atstumas nuo gyvenvietes
			textField_2 = new JTextField();
			textField_2.setBounds(279, 24, 86, 20);
			contentPane.add(textField_2);
			textField_2.setColumns(10);
			
			//atstumas nuo ezero
			textField_4 = new JTextField();
			textField_4.setBounds(279, 165, 86, 20);
			contentPane.add(textField_4);
			textField_4.setColumns(10);
			
			lblAtstumasNuoEero = new JLabel("Atstumas nuo e\u017Eero");
			lblAtstumasNuoEero.setBounds(40, 168, 142, 14);
			contentPane.add(lblAtstumasNuoEero);
			
			lblM = new JLabel("m");
			lblM.setBounds(375, 89, 46, 14);
			contentPane.add(lblM);
			
			label = new JLabel("m");
			label.setBounds(192, 89, 46, 14);
			contentPane.add(label);
			
			label_1 = new JLabel("m");
			label_1.setBounds(375, 27, 46, 14);
			contentPane.add(label_1);
			
			label_2 = new JLabel("m");
			label_2.setBounds(375, 168, 46, 14);
			contentPane.add(label_2);
			
			lblUpstvekinioPlotas = new JLabel("minimalus Upės/Tvekinio plotas");
			lblUpstvekinioPlotas.setBounds(40, 127, 142, 14);
			contentPane.add(lblUpstvekinioPlotas);
			
			//upes/ tvekinio plotas
			textField_5 = new JTextField();
			textField_5.setBounds(279, 124, 86, 20);
			contentPane.add(textField_5);
			textField_5.setColumns(10);
			
			lblM_1 = new JLabel("m^2");
			lblM_1.setBounds(375, 127, 46, 14);
			contentPane.add(lblM_1);
			
			
			/*String[] cities = { "Kuršėnai", "Micaičiai", "Pakumulšiai", "Dirvonėnai"};
			JComboBox comboBox = new JComboBox(cities);
			comboBox.setBounds(204, 54, 161, 20);
			contentPane.add(comboBox);*/
		}
		
		
		
	
	
}
