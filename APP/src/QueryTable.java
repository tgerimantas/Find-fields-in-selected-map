

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFactorySpi;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.Query;
import org.geotools.data.postgis.PostgisNGDataStoreFactory;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.FeatureCollections;
import org.geotools.feature.FeatureIterator;
import org.geotools.filter.text.cql2.CQL;
import org.geotools.filter.text.cql2.CQLException;
import org.geotools.map.Layer;
import org.geotools.swing.action.SafeAction;
import org.geotools.swing.data.JDataStoreWizard;
import org.geotools.swing.table.FeatureCollectionTableModel;
import org.geotools.swing.wizard.JWizard;
import org.opengis.feature.Feature;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.type.FeatureType;
import org.opengis.filter.Filter;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Point;

import org.opengis.filter.FilterFactory2;
import org.opengis.filter.identity.FeatureId;


public class QueryTable extends JFrame {
	// private DataStore dataStore;
	private DataStore dataStore;
	public  JComboBox featureTypeCBox;
	private JTable table;
	private JTextField text;
	private FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2(null);

	public QueryTable() {

		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		getContentPane().setLayout(new BorderLayout());

		text = new JTextField(80);
		text.setText("include");
		text.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				try {
					String textt = text.getText();
					if(textt.equals("")){
						filterFeatures("include");
					}else{
						filterFeatures(textt);
					}
					
				} catch (Exception e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				
			
			}
			
			
		});
		getContentPane().add(text, BorderLayout.NORTH);

		table = new JTable();
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		table.setModel(new DefaultTableModel(5, 5));
		table.setPreferredScrollableViewportSize(new Dimension(500, 200));

		JScrollPane scrollPane = new JScrollPane(table);
		getContentPane().add(scrollPane, BorderLayout.CENTER);

		JMenuBar menubar = new JMenuBar();
		setJMenuBar(menubar);
		featureTypeCBox = new JComboBox();
		
		
		/*featureTypeCBox.addItemListener( new ItemListener(){

			@Override
			public void itemStateChanged(ItemEvent arg0) {
				System.out.println("sadasd     "+QueryTable.featureTypeCBox.getSelectedItem());
				if(QueryTable.featureTypeCBox.getSelectedItem() != null){
				try {
					
						allFeatures();
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}}
			
		});
		*/
		menubar.add(featureTypeCBox);
		
		JMenu dataMenu = new JMenu("Funkcijos");
		menubar.add(dataMenu);
		pack();

		dataMenu.add(new SafeAction("Gauti visus atributus") {
			public void action(ActionEvent e) throws Throwable {
				filterFeatures("include");
			}
		});
		
		/*dataMenu.add(new SafeAction("Kiek išviso atributų") {
			public void action(ActionEvent e) throws Throwable {
				countFeatures();
			}
		});
		dataMenu.add(new SafeAction("Get geometry") {
			public void action(ActionEvent e) throws Throwable {
				queryFeatures();
			}
		});*/
		dataMenu.addSeparator();
		
		dataMenu.add(new SafeAction("Gauti pažymėtus žemėlapyje atributus") {
			public void action(ActionEvent e) throws Throwable {
				filterFromIDs(SelectionLab.IDs);

			}
		});

		dataMenu.addSeparator();

		dataMenu.add(new SafeAction("Parodyti žemėlapyje atributus") {
			public void action(ActionEvent e) throws Throwable {
				try {
                    if  (table.getSelectedRowCount() != 0) {
                    	showSelectedFeaturesOnMap();
                    } else {
                        JOptionPane.showMessageDialog(null, "Pažymėkite lentelės eilutes");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
				
			}
		});
	}

	public void allFeatures() throws Exception {

		
		if(DataStores.mapData == null){
			System.out.println("source  dasda"+ DataStores.mapData);
		}
		String typeName = (String) featureTypeCBox.getSelectedItem();
		System.out.println("source "+ DataStores.mapData.get(typeName));
		SimpleFeatureSource source = DataStores.mapData.get(typeName);
		
		SimpleFeatureCollection features = source.getFeatures();
		System.out.println(features.getID());
		System.out.println(features.getBounds());
		System.out.println(features.getSchema());
		FeatureCollectionTableModel tableModel = new FeatureCollectionTableModel(
		features);
		table.setModel(tableModel);
	}

	private void filterFeatures(String string) {
		String typeName = (String) featureTypeCBox.getSelectedItem();
		SimpleFeatureSource source = DataStores.mapData.get(typeName);

		Filter filter;
		try {
			filter = CQL.toFilter(string);
			SimpleFeatureCollection features;
			try {
				features = source.getFeatures(filter);
				FeatureCollectionTableModel model = new FeatureCollectionTableModel(features);
				table.setModel(model);

			} catch (IOException e) {
				
			}
		} catch (CQLException e) {
			JOptionPane.showMessageDialog(null, "Blogas įvdedimo textas. Įveskite 'include' arba 'stulpelio pavadinimas = elemento pavadinimas'  ");
		}
		
	}

	private void filterFromIDs(Set<FeatureId> ids) throws Exception {
		String typeName = (String) featureTypeCBox.getSelectedItem();
		SimpleFeatureSource source = DataStores.mapData.get(typeName);
		Filter filter = ff.id(ids);
		SimpleFeatureCollection features = source.getFeatures(filter);
		FeatureCollectionTableModel model = new FeatureCollectionTableModel(features);
		table.setModel(model);
	}


	private void countFeatures() throws Exception {
		String typeName = (String) featureTypeCBox.getSelectedItem();
		SimpleFeatureSource source = DataStores.mapData.get(typeName);

		Filter filter = CQL.toFilter(text.getText());
		SimpleFeatureCollection features = source.getFeatures(filter);

		int count = features.size();
		JOptionPane.showMessageDialog(text, "Number of selected features:"+ count);
	}

	private void queryFeatures() throws Exception {
		String typeName = (String) featureTypeCBox.getSelectedItem();
		SimpleFeatureSource source = DataStores.mapData.get(typeName);

		FeatureType schema = source.getSchema();
		String name = schema.getGeometryDescriptor().getLocalName();

		Filter filter = CQL.toFilter(text.getText());

		Query query = new Query(typeName, filter, new String[] { name });

		SimpleFeatureCollection features = source.getFeatures(query);

		FeatureCollectionTableModel model = new FeatureCollectionTableModel(features);
		table.setModel(model);
	}
	
	private void showSelectedFeaturesOnMap() throws IOException {
		
		HashSet<FeatureId> ID = new HashSet<FeatureId>();
		SelectionLab.displaySelectedFeatures(ID);
		SimpleFeatureCollection selectedFeatures = null;
	
		
		String typeName = (String) featureTypeCBox.getSelectedItem();
		for (int i = 0 ; i<SelectionLab.mapFrame.getMapContent().layers().size(); i++)
		{
			
			//System.out.println("TypeName"+ typeName);
			//System.out.println("Map Name"+ SelectionLab.mapFrame.getMapContent().layers().get(i).getFeatureSource().getName().toString());
			//System.out.println();
					if( typeName.equals(SelectionLab.mapFrame.getMapContent().layers().get(i).getFeatureSource().getName().toString()))
					{
						selectedFeatures = (SimpleFeatureCollection)SelectionLab.mapFrame.getMapContent().layers().get(i).getFeatureSource().getFeatures();
					}
	
		}
	
		SimpleFeatureIterator iterator = selectedFeatures.features();
 
            while (iterator.hasNext())
            {
                SimpleFeature feature = iterator.next();
                for (int i = 0; i < table.getSelectedRowCount(); i++) {
                if(feature.getIdentifier().getID().equals(table.getValueAt(table.getSelectedRows()[i], 0)))
                {
                	if (!ID.contains(feature.getIdentifier()))
                	{
                		ID.add(feature.getIdentifier());
                	}
                }
                }
            }
        iterator.close();
        SelectionLab.IDs =ID;
	    SelectionLab.getSelectedEnvelope(ID);
		SelectionLab.displaySelectedFeatures(ID);
    }

}
