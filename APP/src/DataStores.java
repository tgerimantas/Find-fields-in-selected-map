

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.map.Layer;
import org.opengis.filter.identity.FeatureId;

public class DataStores {

	public static Map<String, SimpleFeatureSource> mapData = null;
	
	public DataStores() {
		mapData = new HashMap<String, SimpleFeatureSource>();
	}
	
}