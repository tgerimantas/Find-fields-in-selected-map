

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JComboBox;

import org.geotools.data.simple.SimpleFeatureSource;
import org.opengis.filter.Filter;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class GardenTask3 extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JLabel lblIlgis;
	private JLabel lblPlotis;
	private JTextField textField_2;
	private JLabel lblAtstumasNuoVandens;
	private JTextField textField_3;
	private JTextField textField_4;
	private JLabel lblAtstumasNuoEero;
	private JLabel lblM;
	private JLabel label;
	private JLabel label_1;
	private JLabel label_2;
	private JLabel lblUpstvekinioPlotas;
	private JTextField textField_5;
	private JLabel lblM_1;
	public static double atstumasNuoGyvenvietes = 0;
	public static double laukoIlgis = 0;
	public static double laukoPlotis = 0;
	public static double upesPlotas = 0;
	public static double atstumasNuoEzero = 0;
	
	public GardenTask3() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnIekoti = new JButton("Ie\u0161koti");
		btnIekoti.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				atstumasNuoGyvenvietes = Double.parseDouble(textField_2.getText());
				laukoIlgis = Double.parseDouble(textField.getText());
				laukoPlotis = Double.parseDouble(textField_1.getText());
				upesPlotas = Double.parseDouble(textField_5.getText());
				atstumasNuoEzero = Double.parseDouble(textField_4.getText());
				setVisible(false);
				dispose();
				//SelectionLab.calculations(atstumasNuoGyvenvietes, laukoIlgis, laukoPlotis, upesPlotas, atstumasNuoEzero);
			}
		});
		btnIekoti.setBounds(162, 227, 89, 23);
		contentPane.add(btnIekoti);
		
		//ilgis
		textField = new JTextField();
		textField.setBounds(96, 86, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		//plotis
		textField_1 = new JTextField();
		textField_1.setBounds(279, 86, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblSklypoPlotas = new JLabel("Sklypo dydis");
		lblSklypoPlotas.setBounds(40, 61, 71, 14);
		contentPane.add(lblSklypoPlotas);
		
		lblIlgis = new JLabel("Ilgis:");
		lblIlgis.setBounds(40, 86, 46, 14);
		contentPane.add(lblIlgis);
		
		lblPlotis = new JLabel("Plotis:");
		lblPlotis.setBounds(225, 89, 46, 14);
		contentPane.add(lblPlotis);
		
		JLabel lblAtstumasNuoGyvenviets = new JLabel("Atstumas nuo gyvenviet\u0117s");
		lblAtstumasNuoGyvenviets.setBounds(40, 27, 151, 14);
		contentPane.add(lblAtstumasNuoGyvenviets);
		
		//atstumas nuo gyvenvietes
		textField_2 = new JTextField();
		textField_2.setBounds(279, 24, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		//atstumas nuo ezero
		textField_4 = new JTextField();
		textField_4.setBounds(279, 165, 86, 20);
		contentPane.add(textField_4);
		textField_4.setColumns(10);
		
		lblAtstumasNuoEero = new JLabel("Atstumas nuo e\u017Eero");
		lblAtstumasNuoEero.setBounds(40, 168, 142, 14);
		contentPane.add(lblAtstumasNuoEero);
		
		lblM = new JLabel("m");
		lblM.setBounds(375, 89, 46, 14);
		contentPane.add(lblM);
		
		label = new JLabel("m");
		label.setBounds(192, 89, 46, 14);
		contentPane.add(label);
		
		label_1 = new JLabel("km");
		label_1.setBounds(375, 27, 46, 14);
		contentPane.add(label_1);
		
		label_2 = new JLabel("km");
		label_2.setBounds(375, 168, 46, 14);
		contentPane.add(label_2);
		
		lblUpstvekinioPlotas = new JLabel("Upės/Tvekinio plotas");
		lblUpstvekinioPlotas.setBounds(40, 127, 142, 14);
		contentPane.add(lblUpstvekinioPlotas);
		
		//upes/ tvekinio plotas
		textField_5 = new JTextField();
		textField_5.setBounds(279, 124, 86, 20);
		contentPane.add(textField_5);
		textField_5.setColumns(10);
		
		lblM_1 = new JLabel("m^2");
		lblM_1.setBounds(375, 127, 46, 14);
		contentPane.add(lblM_1);
		
		
		/*String[] cities = { "Kuršėnai", "Micaičiai", "Pakumulšiai", "Dirvonėnai"};
		JComboBox comboBox = new JComboBox(cities);
		comboBox.setBounds(204, 54, 161, 20);
		contentPane.add(comboBox);*/
	}
	
	
	
}
